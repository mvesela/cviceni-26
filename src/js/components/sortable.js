import Component from './component';
import AudioPlayer from './media/audio-player';
import SVG from './svg';
import { stopPlayingVideoPlayers } from './media/media';
import { __dispatchEvent } from '../lib/utils';

export default class Sortable extends Component {
  constructor($element) {
    super($element);
    this.$settings = document.querySelector(
      `[data-sortable-id-reference="${this.target.id}"]`,
    );
    this.feedback = JSON.parse(
      this.$settings.getAttribute('data-module-zpetnavazba'),
    );
    this.$container = this.target;
    this.$items = this.target.querySelectorAll('[data-sortable-drag]');
    this.$droppables = this.target.querySelectorAll('[data-sortable-drop]');
    this.$placeholder = () => document.querySelector('#sortable-placeholder');
    this.$players = this.$container.querySelectorAll('[data-player-audio]');
    this.$inheritors = document.querySelectorAll(
      `[data-sortable-inheritor=${this.target.id}]`,
    );
    this.inheritors = [];
    this.order = [];

    // get direction (type of sortable "horizontalni" or "vertikalni")
    this.direction = this.target.getAttribute('data-sortable-target');
    this.trackingTouch = false;
    this.before = false;
    this.after = false;
    this.evaluation = undefined;

    this.mouse = {
      x: (event) => event.clientX || event.touches[0].clientX,
      y: (event) => event.clientY || event.touches[0].clientY,
      x0: 0,
      y0: 0,
      x1: 0,
      y1: 0,
    };

    this.dragged = {
      el: null,
      clone: null,
      posRel: {},
      rect: () => this.dragged.el.getBoundingClientRect(),
      size: {},
      type: null,
    };

    this.siblings = {
      prev: null,
      next: null,
    };

    this._onStart = this._onStart.bind(this);
    this._onMove = this._onMove.bind(this);
    this._onEnd = this._onEnd.bind(this);
    this._update = this._update.bind(this);

    this._setupInitialOrder();
    this._setupInheritors();
    this._addEventListeners();

    this.cache = {
      colors: []
    };

    // get colors for all feedback variants
    if (this.feedback) {
      this.cache.colors = this.feedback.map(feedback => feedback.barva);
    }
  }

  _onStart(event) {
    if (!event.target.hasAttribute('data-sortable-drag')) {
      return;
    }

    // TODO: zastavit všechno audio on drag start
    if (this.$players.length > 0) {
      stopPlayingVideoPlayers(this.$players);
    }

    this.trackingTouch = true;

    this.dragged.el = event.target;
    this.dragged.type = event.target.getAttribute('data-sortable-drag');
    this.dragged.clone = this.dragged.el.cloneNode(true);
    this.dragged.posRel.top = this.dragged.el.offsetTop;
    this.dragged.posRel.left = this.dragged.el.offsetLeft;
    this.dragged.size.width = this.dragged.el.offsetWidth;
    this.dragged.size.height = this.dragged.el.offsetHeight;

    this.dragged.el.style.top = `${this.dragged.posRel.top}px`;
    this.dragged.el.style.left = `${this.dragged.posRel.left}px`;
    this.dragged.el.style.width = `${this.dragged.size.width}px`;
    this.dragged.el.style.height = `${this.dragged.size.height}px`;
    this.dragged.el.classList.add('is-dragged');

    this.$container.classList.add('is-dragged');

    this._addPlaceholder(this.dragged.el);
    this._onOver();

    this.mouse.x0 = this.mouse.x(event);
    this.mouse.y0 = this.mouse.y(event);

    event.preventDefault();
  }

  _onMove(event) {
    if (!this.trackingTouch) {
      return;
    }

    this.mouse.x1 = this.mouse.x(event) - this.mouse.x0;
    this.mouse.y1 = this.mouse.y(event) - this.mouse.y0;

    window.requestAnimationFrame(this._update);

    this._onOver();
  }

  _onEnd(event) {
    if (!this.trackingTouch) {
      return;
    }

    this.trackingTouch = false;
    this.$container.classList.remove('is-dragged');
    this.dragged.el.classList.remove('is-dragged');
    this.dragged.el.style.top = '';
    this.dragged.el.style.left = '';
    this.dragged.el.style.width = '';
    this.dragged.el.style.height = '';
    this.dragged.el.style.transform = '';

    const $placeholder = this.$placeholder();
    const rect = $placeholder.getBoundingClientRect();

    switch (this.direction) {
      case 'horizontal':
        if (rect.left <= this.mouse.x(event) <= rect.right) {
          this._onDrop();
        }
        break;
      case 'vertical':
        if (rect.top <= this.mouse.y(event) <= rect.bottom) {
          this._onDrop();
        }
        break;

      default:
        break;
    }

    this._removePlaceholder();
    this._updateOrder();
    this._updateOrderInInheritors();
    this._dispatchEvent();
    this._checkAndResolveFeedback();
  }

  // _onEnter() {}

  _onOver() {
    this.before = false;
    this.after = false;

    this._getReference();

    if (
      this.siblings.next
      && this.siblings.next.hasAttribute('data-sortable-drop')
    ) {
      const next = this.siblings.next.getBoundingClientRect();

      switch (this.direction) {
        case 'horizontal':
          if (this.dragged.rect().right > next.left + next.width / 1.5) {
            this.after = true;
            this._addPlaceholder(this.siblings.next);
          }
          break;
        case 'vertical':
          if (this.dragged.rect().bottom > next.top + next.height / 1.5) {
            this.after = true;
            this._addPlaceholder(this.siblings.next);
          }
          break;

        default:
          break;
      }
    }

    if (
      this.siblings.prev
      && this.siblings.prev.hasAttribute('data-sortable-drop')
    ) {
      const prev = this.siblings.prev.getBoundingClientRect();

      switch (this.direction) {
        case 'horizontal':
          if (this.dragged.rect().left < prev.right - prev.width / 1.5) {
            this.before = true;
            this._addPlaceholder(this.siblings.prev);
          }
          break;
        case 'vertical':
          if (this.dragged.rect().top < prev.bottom - prev.height / 1.5) {
            this.before = true;
            this._addPlaceholder(this.siblings.prev);
          }
          break;

        default:
          break;
      }
    }
  }

  // _onLeave() {}

  _onDrop() {
    const $placeholder = this.$placeholder();

    this._addEventListenerToItem(this.dragged.clone);
    // TODO: přiřadit eventListenery audio playerům
    if (this.dragged.clone.querySelector('[data-player-audio]')) {
      AudioPlayer.addEventListeners(this.dragged.clone, this.showFeedback);
    }

    this.$container.insertBefore(this.dragged.clone, $placeholder);
    this.dragged.el.parentElement.removeChild(this.dragged.el);
  }

  _update() {
    this.dragged.el.style.transform = `translate3d(${this.mouse.x1}px, ${this.mouse.y1}px, 0) rotate(5deg)`;
  }

  _addPlaceholder(item) {
    this._removePlaceholder();

    const $placeholder = document.createElement('div');
    $placeholder.classList.add('sortable-item', 'sortable-placeholder');
    $placeholder.setAttribute('id', 'sortable-placeholder');
    $placeholder.style.minHeight = `${this.dragged.size.height}px`;

    if (this.dragged.el.querySelector('img')) {
      const src = this.dragged.el.querySelector('img').getAttribute('src');
      const img = document.createElement('img');
      img.setAttribute('src', src);
      $placeholder.appendChild(img);
    }

    if (this.after) {
      this.$container.insertBefore($placeholder, item.nextElementSibling);
    } else if (this.before) {
      this.$container.insertBefore($placeholder, item);
    } else {
      this.$container.insertBefore($placeholder, item);
    }
  }

  _removePlaceholder() {
    const $placeholder = this.$placeholder();
    if ($placeholder) $placeholder.parentElement.removeChild($placeholder);
  }

  _getReference() {
    const $placeholder = this.$placeholder();
    this.siblings.prev =
      $placeholder.previousElementSibling === this.dragged.el
        ? this.dragged.el.previousElementSibling
        : $placeholder.previousElementSibling;
    this.siblings.next =
      $placeholder.nextElementSibling === this.dragged.el
        ? this.dragged.el.nextElementSibling
        : $placeholder.nextElementSibling;
  }

  _addEventListenerToItem($item) {
    // touch events
    $item.addEventListener('touchstart', this._onStart, false);
    // mouse events
    $item.addEventListener('mousedown', this._onStart, false);

    if (this.dragged.type === 'svg') {
      const svg = new SVG($item.querySelector('[data-svg-target]'));
    }
  }

  _addEventListeners() {
    // touch events
    document.addEventListener('touchmove', this._onMove);
    document.addEventListener('touchend', this._onEnd);
    // mouse events
    document.addEventListener('mousemove', this._onMove);
    document.addEventListener('mouseup', this._onEnd);

    this.$items.forEach(($item) => {
      this._addEventListenerToItem($item);
    });
  }

  // Methods for check if the array is sorted or not
  // Return True: array is in the right state, False: array is NOT in the right state
  // Array looks like: [1,2,3,4]
  // eslint-disable-next-line class-methods-use-this
  checkIfTheArrayIsGood(array) {
    // condition for function some(), return if there is item in array which is not in right position
    const elementInWrongPosition = (element, index) => index !== (element - 1);
    // all has to be negate, to return the right condition (true: sorted, false:not sorted)
    return !array.some(elementInWrongPosition);
  }

  // Method which check how many steps needs to be done to sort array
  // Return: 0 -> 0 steps, array is sorted
  //        1 -> 1 steps is needed for sort the array
  //        2 -> 2 and more steps is needed for sort the array
  // Array looks like: [1,2,3,4]
  oneMove(array) {
    if (this.checkIfTheArrayIsGood(array)) return 0; // Check if the array is sorted
    for (let i = 0; i < array.length; i += 1) {
      if (array[i] !== i + 1) {
        const changedArray = [...array];
        const element = changedArray[i];
        changedArray.splice(i, 1);
        changedArray.splice(element - 1, 0, element);

        if (this.checkIfTheArrayIsGood(changedArray)) return 1; // Check if the array after 1 step is sorted
      }
    }
    return 2; // In every other results the array needs more than 2 steps to be sorted
  }

  // After interaction with modul razeni this method is called to resolve a feedback button
  _checkAndResolveFeedback() {
    if (this.feedback) {
      // Get the array of items (gets only their correct position - in int), example -> [1,3,4,2]
      const arrayOfItems = [
        ...this.target.querySelectorAll('[data-sortable-drag]'),
      ].map((item) => parseInt(item.dataset.sortableSpravnaodpoved, 10));

      // variable to map a feedback which is got from condition in JSON
      let feedbackResult = 0;

      // Check if the zpetnaVazba is 2 or 3 conditional
      if (this.feedback.length === 2) {
        if (!this.checkIfTheArrayIsGood(arrayOfItems)) {
          feedbackResult = 1;
        }
      }
      else {
        feedbackResult = this.oneMove(arrayOfItems);
      }

      // map feedback via feedbackResult
      const feedbackActual = this.feedback.find((feedback) => feedback.podminka === feedbackResult);

      this.$buttonFeedback.classList.remove(...this.cache.colors);
      this.$buttonFeedback.classList.add(feedbackActual.barva);
      this.$buttonFeedbackText.innerText = feedbackActual.text;
      this.evaluation = feedbackActual.splneno;
    }
    this.showFeedback();
  }

  _setupInitialOrder() {
    this._updateOrder();
  }

  _setupInheritors() {
    if (this.$inheritors.length > 0) {
      this.inheritors = Array.from(this.$inheritors).map(($inheritor) => ({
        $inheritor,
        $container: $inheritor.querySelector('[data-sortable-inheritor-item]')
          .parentElement,
      }));
    }
  }

  _updateOrder() {
    this.order = Array.from(
      this.target.querySelectorAll('[data-sortable-drag]'),
    ).map(($item) => ({ id: parseInt($item.dataset.sortableIndex, 10) }));
  }

  _updateOrderInInheritors() {
    if (this.$inheritors.length > 0) {
      this.inheritors.forEach((inheritor) => {
        this.order.forEach((item) => {
          inheritor.$container.appendChild(
            inheritor.$container.querySelector(
              `[data-sortable-inheritor-item="${item.id}"]`,
            ),
          );
        });
      });
    }
  }

  _dispatchEvent() {
    __dispatchEvent(
      this.target,
      'sortable.change',
      {},
      {
        id: this.target.id,
        evaluation: this.evaluation,
        order: this.order,
      },
    );
  }
}
