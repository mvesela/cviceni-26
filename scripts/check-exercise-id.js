// dependecies
const { readdirSync, statSync } = require('fs');
const { extname, join } = require('path');
const pkg = require('../package.json');

const isFile = (path) => statSync(path).isFile();

const isJson = (path) => extname(path) === '.json';

const getJsons = (path) => readdirSync(path)
  .map((name) => join(path, name))
  .filter(isFile)
  .filter(isJson);

const jsons = getJsons(`${process.cwd()}/src/data/`);
const ids = [];
const exercises = [];

//
// TEST 1.
// number equality
console.log('=====', 'Test: Number equality');
console.log(pkg.exercise.number, 'exercise.number in package.json');
console.log(jsons.length, 'JSON files');
if (pkg.exercise.number !== jsons.length) {
  console.error(
    `× exercise.number is ${pkg.exercise.number > jsons.length ? 'higher' : 'lower'}.`,
    `Change it to ${jsons.length}!`,
  );
} else {
  console.error('✓ Ok.'.toUpperCase());
}
console.log('-----');

//
// TEST 2.
// duplicates
console.log('=====', 'Test: Duplicated ID');

let duplicated = false;

jsons.forEach((json) => {
  const data = require(json);
  const exercise = {
    id: data.cviceni.id,
    slug: data.cviceni.slug,
    duplicatedId: ids.includes(data.cviceni.id),
  };
  exercises.push(exercise);

  // console.log(exercise);

  ids.push(exercise.id);

  if (exercise.duplicatedId) {
    duplicated = true;
    console.error(exercise.id, exercise.slug, exercise.duplicatedId);
  }
});

if (duplicated === false) {
  console.error('✓ Ok.'.toUpperCase());
  console.log('-----');
}

// console.log(exercises.sort((a, b) => a.id - b.id))
